import numpy as np
import matplotlib.pyplot as plt

def mandelbrot(c, max_iter, exponent):
    z = 0
    for n in range(max_iter):
        if abs(z) > 2:
            return n
        z = z**exponent + c
    return max_iter

def mandelbrot_set(xmin, xmax, ymin, ymax, width, height, max_iter, exponent):
    x = np.linspace(xmin, xmax, width)
    y = np.linspace(ymin, ymax, height)
    X, Y = np.meshgrid(x, y)
    c = X + 1j * Y
    mandelbrot_image = np.zeros((height, width))
    for i in range(height):
        for j in range(width):
            mandelbrot_image[i, j] = mandelbrot(c[i, j], max_iter, exponent)
    return mandelbrot_image

def plot_mandelbrot(xmin, xmax, ymin, ymax, width, height, max_iter, exponent):
    mandelbrot_img = mandelbrot_set(xmin, xmax, ymin, ymax, width, height, max_iter, exponent)
    
    plt.figure(figsize=(10, 8))
    plt.imshow(mandelbrot_img, extent=[xmin, xmax, ymin, ymax], cmap='hot')
    plt.colorbar(label='Broj iteracija')
    plt.title("Mandelbrotov Skup")
    plt.xlabel("Re(c)")
    plt.ylabel("Im(c)")
    plt.show()

#Rucna promjena parametara
xmin = -2.0
xmax = 1.0
ymin = -1.5
ymax = 1.5
width = 1000
height = 800
max_iter = 500
exponent = 2

plot_mandelbrot(xmin, xmax, ymin, ymax, width, height, max_iter, exponent)