# chaos

Aplikacija koristi numeričke metode za analizu dinamičkih sustava i kompleksnih skupova s implementacijom u Pythonu. Pruža interaktivne grafove za vizualizaciju bifurkacijskih dijagrama, Mandelbrotovih skupova, RLC spojeva i istosmjernih uzlaznih pretvarača. Korisnici mogu mijenjati parametre u stvarnom vremenu i promatrati rezultate, čime se potiče dublje razumijevanje i istraživački duh. Aplikacija je dizajnirana za studente, istraživače i inženjere, omogućujući im intuitivno i angažirano učenje kroz neposredno iskustvo i eksperimentiranje. Ovaj alat čini učenje dinamičnih sustava zanimljivijim i dostupnijim.


## Instalacija

1. Klonirajte repozitorij:

    git clone https://gitlab.com/vubhr/projekti/chaos.git

2. Uđite u direktorij projekta:

    cd chaos

3. Provjerite da imate instaliran Python. Možete ga preuzeti sa službene stranice: https://www.python.org/downloads/

4. Instalirajte sve potrebne pakete:

    pip install -r requirements.txt

5. Pokrenite željeni modul:

    python modul.py


### Autori

Tomislav Adamović, Iva Stanković


### Perspektive i mogućnosti za buduću upotrebu

Proširiti repozitorij primjenom naprednih alata za analizu podataka, poput strojnog učenja i umjetne inteligencije. Ove tehnologije mogu dodatno unaprijediti analizu dinamičkih sustava i kompleksnih skupova, omogućavajući automatsko prepoznavanje obrazaca, optimizaciju parametara i predikciju ponašanja sustava. Implementacija ovih naprednih alata otvorila bi nove mogućnosti za istraživanje i primjenu u različitim znanstvenim i inženjerskim disciplinama.
