import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

#Definiranje globalnih varijabli za parametre
R = 0.73     # 0.73 Ω
L = 698e-6   # 698 µH
C = 470e-6   # 470 µF
Rd = 56      # 56 Ω
Uref = 5     # 5 V
E = 16       # 16 V
T = 500e-6   # 500 µs
sim_time = 20 * T  # 60T
h = 50e-9    # 50 ns

def iup(y, t, E, R, L, C, Rd, T):
    vc, ic = y
    switch = int((t % T) < (T / 2))
    dvc_dt = (1 / C) * (ic - vc / Rd)
    dic_dt = (1 / L) * ((-vc + E * switch - R * ic))
    return [dvc_dt, dic_dt]

def solve_iup(E, R, L, C, Rd, T, sim_time, h):
    time = np.arange(0, sim_time, h)
    initial_conditions = [0, 0]
    results = odeint(iup, initial_conditions, time, args=(E, R, L, C, Rd, T))
    vc = results[:, 0]
    ic = results[:, 1]
    return time, vc, ic

def plot_iup(time, vc, ic):
    fig, ax1 = plt.subplots(figsize = (10, 6))

    ax1.set_title('Simulacija istosmjernog uzlaznog pretvarača')
    ax1.set_xlabel('Vrijeme (s)')
    ax1.set_ylabel('Struja kroz induktor (A)', color = 'tab:blue')
    ax1.plot(time, ic, label = 'Struja kroz induktor (i_L)', color = 'tab:blue')
    ax1.tick_params(axis = 'y', labelcolor = 'tab:blue')

    ax2 = ax1.twinx()
    ax2.set_ylabel('Napon na kondenzatoru (V)', color = 'tab:red')
    ax2.plot(time, vc, label = 'Napon na kondenzatoru (v_c)', color = 'tab:red')
    ax2.tick_params(axis='y', labelcolor='tab:red')

    fig.tight_layout()
    plt.show()

#Rucna promjena parametara
R = 0.73     # 0.73 Ω
L = 698e-6   # 698 µH
C = 470e-6   # 470 µF
Rd = 56      # 56 Ω
Uref = 5     # 5 V
E = 16       # 16 V
T = 500e-6   # 500 µs
sim_time = 20 * T  # 60T
h = 50e-9    # 50 ns

time, vc, ic = solve_iup(E, R, L, C, Rd, T, sim_time, h)

plot_iup(time, vc, ic)