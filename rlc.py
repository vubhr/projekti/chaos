import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

def rlc(state, t, R, L, C, U0, omega, is_sin):
    i, v = state
    dU = U0 * omega * np.cos(omega*t) if is_sin else U0
    di_dt = v
    dv_dt = (dU - R*v - i/C)/L
    return [di_dt, dv_dt]

def solve_rlc(R, L, C, U0, omega, is_sin, t):
    initial_conditions = [0, U0/C]
    results = odeint(rlc, initial_conditions, t, args=(R, L, C, U0, omega, is_sin))
    i_values = results[:, 0]
    return i_values

def plot_rlc(t, i_values):
    plt.figure(figsize = (10, 6))
    plt.plot(t, i_values, label = 'Struja (i)', color = 'blue')
    plt.title('Dinamika struje u RLC krugu')
    plt.xlabel('Vrijeme (s)')
    plt.ylabel('Struja (A)')
    plt.legend()
    plt.grid(True)
    plt.show()

#Rucna promjena parametara
R = 20
L = 10
C = 1
U0 = 16
omega = 1
is_sin = False  #Postavi na True za sinusoidalni napon

#Rucna promjena vremena simulacije
t = np.linspace(0, 50, 5000)

i_values = solve_rlc(R, L, C, U0, omega, is_sin, t)

plot_rlc(t, i_values)