import numpy as np
import matplotlib.pyplot as plt

def bifurcation_diagram(num_points = 10000, iterations = 1000, last_iterations = 100):
    r_values = np.linspace(2.4, 4.0, num_points)
    x_values = 0.5 * np.ones(num_points)

    r = []
    x = []

    for i in range(iterations):
        x_values = r_values * x_values * (1 - x_values)
        if i >= (iterations - last_iterations):
            r.extend(r_values)
            x.extend(x_values)

    return np.array(r), np.array(x)

def plot_bifurcation_diagram(r, x, r_max):
    plt.figure(figsize = (10, 6))
    plt.scatter(r, x, s = 0.1, color = 'blue', alpha = 0.5)
    plt.title("Bifurkacijski dijagram populacijske jednadžbe")
    plt.xlabel("r")
    plt.ylabel("x")
    plt.xlim(2.4, r_max)
    plt.ylim(0, 1)
    plt.show()

r, x = bifurcation_diagram()

#Rucna promjena r_max vrijednosti
r_max = 4.0

#Rucna promjena broja točaka
num_points = 20000

#Rucna promjena broja iteracija
iterations = 2000

#Rucna promjena broja zadnjih iteracija za crtanje
last_iterations = 200

r, x = bifurcation_diagram(num_points=num_points, iterations=iterations, last_iterations=last_iterations)

plot_bifurcation_diagram(r, x, r_max)